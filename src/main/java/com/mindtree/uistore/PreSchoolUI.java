package com.mindtree.uistore;

import org.openqa.selenium.By;

public class PreSchoolUI {

	//pre school link (click)
	public static By preSchoolLink = By.cssSelector("li.pre-school");
	
	//accepting cache
	public static By acceptingCache = By.id("onetrust-accept-btn-handler");
	
	
	//select diest and nutrition
	public static By dietAndNutrition = By.xpath("//img[@alt='dealing-with-fussy-eaters']");
	
	
	//select child overweight
	public static By overWeight = By.xpath("//img[@alt='I-think-my-child-is-overweight']");
	
	
}
