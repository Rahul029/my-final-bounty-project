package com.mindtree.uistore;

import org.openqa.selenium.By;

public class SearchUI {

	//search field (sendkeywithenter)
	public static By searchField = By.id("searchsite");

	//select item
	public static By selectItem = By.xpath("//img[@alt='MamaMio-TrimesterOne-Bundle']");
	
	
	//get this offer now
	public static By getThisOfferNow = By.xpath("//a[@data-prod-category='Maternity']");
	
	//accepting cache
	public static By acceptingCache = By.id("onetrust-accept-btn-handler");

	
	//select baskeyt
	public static By selectBasket = By.xpath("//span[@class='responsiveFlyoutBasket_icon_container']");
}
