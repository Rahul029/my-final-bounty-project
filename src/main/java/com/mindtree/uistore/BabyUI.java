package com.mindtree.uistore;

import org.openqa.selenium.By;

public class BabyUI {

	//select baby link form home page (click)
	public static By babyLink = By.cssSelector("li.baby");
	
	
	//select sleep and crying baby (click)
	public static By sleepAndCrying = By.xpath("//img[@alt='sleep-and-crying']");
	
	
	//select sleeping fact by age (click)
	public static By sleepingFactByAge = By.xpath("//span[@class='icon-wrapper cf']//child::img[@alt='fast-facts-sleeping-and-crying']");
	
	
	//accept cache
	public static By acceptCache = By.id("onetrust-accept-btn-handler");
	
}
