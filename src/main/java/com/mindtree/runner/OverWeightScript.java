package com.mindtree.runner;

import java.lang.reflect.Method;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.mindtree.pageobject.PreSchool;
import com.mindtree.reusable.ReusableMethod;
import com.mindtree.utility.ExtentReport;

public class OverWeightScript extends BaseClass{

	@BeforeMethod
	public void createTest(Method method) {
		test = report.createTest(this.getClass().getSimpleName() + ":" + method.getName());
	}
	
	@Test(priority=1)
	public void selectPreSchool() {
		ReusableMethod.timelaps(driver);
		boolean result = PreSchool.preSchoolLink(driver, log, test);
		Assert.assertTrue(result);
	}
	
	@Test(priority=2)
	public void acceptCache() {
		ReusableMethod.timelaps(driver);
		boolean result = PreSchool.acceptCache(driver, log, test);
		Assert.assertTrue(result);
	}


	@Test(priority=3)
	public void diestAndNutrition() {
		ReusableMethod.timelaps(driver);
		boolean result = PreSchool.diestAndNutrition(driver, log, test);
		Assert.assertTrue(result);
	}
	
	@Test(priority=4)
	public void overweight() {
		ReusableMethod.timelaps(driver);
		boolean result = PreSchool.overweight(driver, log, test);
		Assert.assertTrue(result);
	}
	

	@AfterMethod
	public void result(ITestResult result) {
		if (result.getStatus() == ITestResult.FAILURE) {
			log.error("Test case Failed is : " + result.getName());
			ExtentReport.ExtentFail(test, "test case failed is : " + result.getName());
			ExtentReport.screenshot(driver, test);
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			log.info("Test case pass is : " + result.getName());
			ExtentReport.ExtentPass(test, "test case pass is : " + result.getName());
			ExtentReport.screenshot(driver, test);
		}
	}
}
