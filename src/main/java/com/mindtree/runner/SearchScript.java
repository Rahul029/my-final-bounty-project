package com.mindtree.runner;

import java.lang.reflect.Method;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.mindtree.pageobject.Search;
import com.mindtree.reusable.ReusableMethod;
import com.mindtree.utility.ExtentReport;


public class SearchScript extends BaseClass{

	@BeforeMethod
	public void createTest(Method method) {
		test = report.createTest(this.getClass().getSimpleName()+ ":" + method.getName());
	}
	
	
	@Test(priority=1, dataProvider="searchItem")
	public void searchItem(String serachItem) {
		ReusableMethod.timelaps(driver);
		boolean result = Search.searchItem(driver, log, test, serachItem);
		Assert.assertTrue(result);
	}
	
	
	
	@Test(priority=2)
	public void acceptCache() {
		ReusableMethod.timelaps(driver);
		boolean result = Search.acceptCache(driver, log, test);
		Assert.assertTrue(result);
	}
	
	@Test(priority=3)
	public void selectItem() {
		ReusableMethod.timelaps(driver);
		boolean result = Search.selectItem(driver, log, test);
		Assert.assertTrue(result);
	}

	@Test(priority=4)
	public void gotOffer() {
		ReusableMethod.timelaps(driver);
		boolean result = Search.getOfferNow(driver, log, test);
		Assert.assertTrue(result);
	}


	@AfterMethod
	public void result(ITestResult result) {
		if(result.getStatus() == ITestResult.FAILURE) {
			log.error("Test case Failed is : " + result.getName());
			ExtentReport.ExtentFail(test, "test case failed is : " + result.getName());
			ExtentReport.screenshot(driver, test);
		}
		else if(result.getStatus() == ITestResult.SUCCESS) {
			log.info("Test case pass is : " + result.getName());
			ExtentReport.ExtentPass(test, "test case pass is : " + result.getName());
			ExtentReport.screenshot(driver, test);
		}
	}
}
