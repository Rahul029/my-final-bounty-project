package com.mindtree.pageobject;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.ExtentTest;
import com.mindtree.reusable.ReusableMethod;
import com.mindtree.uistore.BabyUI;
import com.mindtree.utility.ExtentReport;
import com.mindtree.utility.Log;

public class Baby {

	// selecting baby link
	public static boolean babyLink(WebDriver driver, Logger log, ExtentTest test) {
		if (ReusableMethod.getElement(driver, log, BabyUI.babyLink)) {
			Log.logInfo(log, "baby link found");
			ExtentReport.ExtentInfo(test, "baby link found");
			ReusableMethod.explicitWait(driver,BabyUI.acceptCache);
			if (ReusableMethod.click(driver, log, BabyUI.babyLink)) {
				Log.logInfo(log, "baby link selected ");
				ExtentReport.ExtentInfo(test, "baby link selected");
				return true;
			}
		}
		return false;
	}

	// select sleep and cry baby
	public static boolean sleepAndCryBaby(WebDriver driver, Logger log, ExtentTest test) {
		if (ReusableMethod.getElement(driver, log, BabyUI.sleepAndCrying)) {
			Log.logInfo(log, "sleep And Crying found");
			ExtentReport.ExtentInfo(test, "sleep And Crying found");
			ReusableMethod.explicitWait(driver,BabyUI.acceptCache);
			if (ReusableMethod.click(driver, log, BabyUI.sleepAndCrying)) {
				Log.logInfo(log, "selleping and crying  selected ");
				ExtentReport.ExtentInfo(test, "selleping and crying  selected");
				return true;
			}
		}
		return false;
	}

	// sleeping fact by age
	public static boolean sleepingFactByAge(WebDriver driver, Logger log, ExtentTest test) {
		if (ReusableMethod.getElement(driver, log, BabyUI.acceptCache)) {
			Log.logInfo(log, "cache button found");
			ExtentReport.ExtentInfo(test, "cache button found");
			ReusableMethod.explicitWait(driver,BabyUI.acceptCache);
			if (ReusableMethod.click(driver, log, BabyUI.acceptCache)) {
				Log.logInfo(log, "cache button clicked ");
				if (ReusableMethod.getElement(driver, log, BabyUI.sleepingFactByAge)) {
					Log.logInfo(log, "sleepingFactByAge found");
					ExtentReport.ExtentInfo(test, "sleepingFactByAgefound");
					if (ReusableMethod.click(driver, log, BabyUI.sleepingFactByAge)) {
						Log.logInfo(log, "sleeping Fact By Age  selected ");
						ExtentReport.ExtentInfo(test, "sleeping Fact By Age selected");
						return true;
					}
				}
			}
		}
		return false;
	}

}
