package com.mindtree.pageobject;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.ExtentTest;
import com.mindtree.reusable.ReusableMethod;
import com.mindtree.uistore.PreSchoolUI;
import com.mindtree.utility.ExtentReport;
import com.mindtree.utility.Log;

public class PreSchool {


	// selecting pre school link
	public static boolean preSchoolLink(WebDriver driver, Logger log, ExtentTest test) {
		if (ReusableMethod.getElement(driver, log, PreSchoolUI.preSchoolLink)) {
			Log.logInfo(log, "pre School Link found");
			ExtentReport.ExtentInfo(test, "pre School Link found");
			ReusableMethod.explicitWait(driver,PreSchoolUI.preSchoolLink);
			if (ReusableMethod.click(driver, log, PreSchoolUI.preSchoolLink)) {
				Log.logInfo(log, "preschool  link selected ");
				ExtentReport.ExtentInfo(test, "preschool  link selected");
				return true;
			}
		}
		return false;
	}
	
	//accepting cache
	public static boolean acceptCache(WebDriver driver, Logger log, ExtentTest test) {
		if (ReusableMethod.getElement(driver, log, PreSchoolUI.acceptingCache)) {
			Log.logInfo(log, "accept cache button found");
			ExtentReport.ExtentInfo(test, "accept cache button found");
			ReusableMethod.explicitWait(driver,PreSchoolUI.acceptingCache);
			if (ReusableMethod.click(driver, log, PreSchoolUI.acceptingCache)) {
				Log.logInfo(log, "accept cache button clicked ");
				ExtentReport.ExtentInfo(test, "accept cache button clicked");
				return true;
			}
		}
		return false;
	}
	
	//select diet and nutrition
	public static boolean diestAndNutrition(WebDriver driver, Logger log, ExtentTest test) {
		if (ReusableMethod.getElement(driver, log, PreSchoolUI.dietAndNutrition)) {
			Log.logInfo(log, "diet And Nutrition link found");
			ExtentReport.ExtentInfo(test, "diet And Nutrition link foun");
			ReusableMethod.explicitWait(driver,PreSchoolUI.dietAndNutrition);
			if (ReusableMethod.click(driver, log, PreSchoolUI.dietAndNutrition)) {
				Log.logInfo(log, "diet And Nutrition link clicked ");
				ExtentReport.ExtentInfo(test, "diet And Nutrition link clicked");
				return true;
			}
		}
		return false;
	}
	
	
	//overweight 
	public static boolean overweight(WebDriver driver, Logger log, ExtentTest test) {
		if (ReusableMethod.getElement(driver, log, PreSchoolUI.overWeight)) {
			Log.logInfo(log, "overWeightlink found");
			ExtentReport.ExtentInfo(test, "overWeight link foun");
			ReusableMethod.explicitWait(driver,PreSchoolUI.overWeight);
			if (ReusableMethod.click(driver, log, PreSchoolUI.overWeight)) {
				Log.logInfo(log, "over weight link clicked ");
				ExtentReport.ExtentInfo(test, "over weight link clicked ");
				return true;
			}
		}
		return false;
	}
	
}
