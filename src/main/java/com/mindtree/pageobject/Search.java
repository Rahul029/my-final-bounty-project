package com.mindtree.pageobject;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.ExtentTest;
import com.mindtree.reusable.ReusableMethod;
import com.mindtree.uistore.SearchUI;
import com.mindtree.utility.ExtentReport;
import com.mindtree.utility.Log;

public class Search {

	// searching item
	public static boolean searchItem(WebDriver driver, Logger log, ExtentTest test, String searchItemName) {
		if (ReusableMethod.getElement(driver, log, SearchUI.searchField)) {
			Log.logInfo(log, "search field found");
			ExtentReport.ExtentInfo(test, "search field found");
			if (ReusableMethod.sendkeyWithEnter(driver, SearchUI.searchField, searchItemName)) {
				Log.logInfo(log, "name entered int the search field ");
				ExtentReport.ExtentInfo(test, "sname entered int the search field");
				return true;
			}
		}
		return false;
	}

	// accept cache
	public static boolean acceptCache(WebDriver driver, Logger log, ExtentTest test) {
		if (ReusableMethod.getElement(driver, log, SearchUI.acceptingCache)) {
			Log.logInfo(log, "accept cache button found");
			ExtentReport.ExtentInfo(test, "accept cache button found");
			ReusableMethod.explicitWait(driver, SearchUI.acceptingCache);
			if (ReusableMethod.click(driver, log, SearchUI.acceptingCache)) {
				Log.logInfo(log, "accept cache button clicked ");
				ExtentReport.ExtentInfo(test, "accept cache button clicked");
				return true;
			}
		}
		return false;
	}

	// selecting item
	public static boolean selectItem(WebDriver driver, Logger log, ExtentTest test) {
		if (ReusableMethod.getElement(driver, log, SearchUI.selectItem)) {
			Log.logInfo(log, "item is found ");
			ExtentReport.ExtentInfo(test, "item is found");
			if (ReusableMethod.click(driver, log, SearchUI.selectItem)) {
				Log.logInfo(log, "item is selectde ");
				ExtentReport.ExtentInfo(test, "item is selectde ");
				return true;
			}
		}
		return false;
	}

	// getOfer now

	public static boolean getOfferNow(WebDriver driver, Logger log, ExtentTest test) {
		if (ReusableMethod.getElement(driver, log, SearchUI.getThisOfferNow)) {
			Log.logInfo(log, "get this offer noew  is found ");
			ExtentReport.ExtentInfo(test, "get this offer noew  is found ");
			ReusableMethod.explicitWait(driver,  SearchUI.getThisOfferNow);
			if (ReusableMethod.click(driver, log, SearchUI.getThisOfferNow)) {
				Log.logInfo(log, "get this offer noew  is clicked ");
				ExtentReport.ExtentInfo(test, "get this offer noew  is clicked  ");
				ReusableMethod.explicitWait(driver,  SearchUI.getThisOfferNow);
				
				//handling window
				ArrayList<String> list = new ArrayList<String>(driver.getWindowHandles());
				driver.switchTo().window(list.get(1));

				if (ReusableMethod.getElement(driver, log, SearchUI.selectBasket)) {
					Log.logInfo(log, "basket found ");
					ExtentReport.ExtentInfo(test, "basket found ");
					if (ReusableMethod.click(driver, log, SearchUI.selectBasket)) {
						Log.logInfo(log, "basket is clicked ");
						ExtentReport.ExtentInfo(test, "basket  is clicked  ");

						return true;
					}
				}
			}
		}
		return false;
	}

}
