package com.mindtree.utility;

import java.io.File;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.google.common.io.Files;
import com.mindtree.reusable.ReusableMethod;

public class ExtentReport {

	private static String path;
	private static ExtentHtmlReporter reporter;
	
	
	public static ExtentReports report() {
		path = System.getProperty("user.dir") +  PropertyFileReader.loadFile().getProperty("report") + 
				PropertyFileReader.loadFile().getProperty("className") + 
				"_" + ReusableMethod.setDateTime() + ".html";
		
		reporter = new ExtentHtmlReporter(path);
		ExtentReports report = new ExtentReports();
		report.attachReporter(reporter);
		return report;
	}
	
	
	public static void screenshot(WebDriver driver, ExtentTest test) {
		TakesScreenshot ss = (TakesScreenshot) driver;
		File srcFile = ss.getScreenshotAs(OutputType.FILE);
		
		String path = System.getProperty("user.dir") 
				     + PropertyFileReader.loadFile().getProperty("screenshot")
				     +  PropertyFileReader.loadFile().getProperty("className")
				     + ReusableMethod.setDateTime() + ".png";
		
		File destFile = new File(path);
		
		try {
			Files.copy(srcFile, destFile);
			test.addScreenCaptureFromPath(destFile.toString());
		} catch (Exception e ) {
			e.printStackTrace();
		}
 	}
	
	
	public static void ExtentPass(ExtentTest test, String message) {
		test.log(Status.PASS, message);
	}
	
	public static void ExtentFail(ExtentTest test, String message) {
		test.log(Status.FAIL, message);
	}
	
	public static void ExtentSkip(ExtentTest test, String message) {
		test.log(Status.SKIP, message);
	}
	
	
	public static void ExtentInfo(ExtentTest test, String message) {
		test.log(Status.INFO, message);
	}
	
	

	public static String getFile()
	{
		return path;
	}
	
}
