package com.mindtree.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;

public class ExcelReader {

	public static Object[][] readExcel(String excelPath, String excelFile, String sheetName) throws IOException {
		Object[][] list = null;

		File file = new File(excelPath);
		FileInputStream input = new FileInputStream(file);

		HSSFWorkbook wb = new HSSFWorkbook(input);
		HSSFSheet sheet = wb.getSheet(sheetName);
		Row row = sheet.getRow(0);

		list = new Object[sheet.getLastRowNum()][row.getLastCellNum()];
		for (int i = 0; i < sheet.getLastRowNum(); i++) {
			for (int j = 0; j < row.getLastCellNum(); j++) {
				list[i][j] = sheet.getRow(i + 1).getCell(j).toString();
			}

		}

		return list;

	}
}
