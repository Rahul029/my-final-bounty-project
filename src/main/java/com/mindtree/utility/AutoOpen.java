package com.mindtree.utility;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.mindtree.reusable.ReusableComponent;

public class AutoOpen {

	public static void autoOpen(Logger log) {
		String path =  ExtentReport.getFile();
		WebDriver driver = ReusableComponent.getDriver(log);
		log.info("auto opening extent report");
		driver.get(path);
	}
	
}
