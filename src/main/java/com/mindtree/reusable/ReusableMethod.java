package com.mindtree.reusable;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mindtree.utility.Log;
import com.mindtree.utility.PropertyFileReader;


public class ReusableMethod {

	public static void timelaps(WebDriver driver) {
		int itme = Integer.parseInt(PropertyFileReader.loadFile().getProperty("wait"));
		driver.manage().timeouts().implicitlyWait(itme, TimeUnit.SECONDS);

	}

	public static boolean loadUrl(WebDriver driver, Logger log) {

		try {
			driver.get(PropertyFileReader.loadFile().getProperty("url"));
			Log.logInfo(log, "website loaded");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;

	}


	public static String setDateTime() {
		DateTimeFormatter dateTime = DateTimeFormatter.ofPattern("yyyy_mm_dd_hh_mm_ss");
		LocalDateTime now = LocalDateTime.now();
		
		return dateTime.format(now);
	}
	
	
	public static boolean getElement(WebDriver driver, Logger log, By selector) {
		try {
			driver.findElement(selector);
			log.debug("selectro" + selector +"found");
			return true;
		} catch (Exception e) {
		log.error("selector" + selector + "not founbd");
		e.printStackTrace();
	}
		return false;
	}
	
	
	public static boolean click(WebDriver driver, Logger log, By selector) {
		try {
			driver.findElement(selector).click();
			log.debug("selector" + selector + "found");
			return true;
		} catch(Exception e) {
			log.error("selector" + selector + "not found");
			e.printStackTrace();
		}
		return false;
		
	}
	
	
	public static boolean sendkey(WebDriver driver, By searchbox, String string) {
		try {
			driver.findElement(searchbox).sendKeys(string);
			return true;
		} catch(Exception e) {
			System.out.println("unable to enter data");
		}
		return false;
		
	}
	
	
	public static boolean sendkeyWithEnter(WebDriver driver, By searchbox, String string) {
		try {
			WebElement ele = driver.findElement(searchbox);
			ele.sendKeys(string);
			ele.sendKeys(Keys.ENTER);
			return true;
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("unable to enter data");
		}
		return false;
		
	}
	
	public static boolean explicitWait(WebDriver driver, By selector) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(selector));
		return true;
	}
	
	
	
	
	
	
	
	

}
